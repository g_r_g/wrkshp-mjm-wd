QUARANTYPE

ABOUT
Quarantype is a collaborative font designed during the Covid-19 lockdown. The font is available in two styles: Regular and Oblique.
Designed in 2020 by Alain Foncier, Anton Maous, arthurecho, Audrey, Benoît Sans Terre, Chris Silverman, Christian Bolmont, Christophe Gaudard, Clara Sambot, Clément Castoi, coco_du_92, Dano N'dino, dr_adri, El monstruo de los burritos, Flogio, Grosse Moula, gtnthrn, Guito, Herr-Max, Isia Poutine Prince, Jacob Youngblood, Jost Patchouli, Jules Tirilly, Keussel, Kyril Malouk, lazy dog, louis souêtre, louix, Marielle Nils, Maureen Leprêtre, Nicolas "Cube" Bardey, Novice Jaune, paul bouigue, Paul bouniot, Paul Prepper, Pauline Le Pape, Paupiette, Phil Covid aka Lil Covid Vert, Philippe Edouard, Robert Daignault, Romain Guillo, Ruyé, Supercapibara, Teddy Jemancogne, Thomas Carlotti, ultra batard, Victor Coupaud, William Morris, Yuriga Garine, Yorgana Le Wintourec and fonderie.download team, published in may 2020 by fonderie.download.

LICENSE
Quarantype is published under the WTFPL – Do What the Fuck You Want to Public License. For more informations, open the LICENSE.txt file or go to http://www.wtfpl.net/.

ABOUT FONDERIE.DOWNLOAD
We design and distribute typefaces. All our fonts are released under the WTFPL license.

CONTACT
http://www.fonderie.download/
contact@fonderie.download
