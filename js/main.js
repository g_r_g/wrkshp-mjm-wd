let btn = $("#modeBtn");
btn.click(() => {
  $("#tuning").toggleClass("offtuning");
  $("#tuning").toggleClass("ontuning");
  $("#soft").toggleClass("onsoft");
  $("#soft").toggleClass("offsoft");
});

setInterval(() => {
  let randImg = [
    "christin-hume-6Dlcufknp58-unsplash.jpg",
    "marek-piwnicki-6vddaknKm3k-unsplash.jpg",
    "marek-piwnicki-dBCcvcVbJjw-unsplash.jpg",
    "rktkn-BZ9z9ZeUjbI-unsplash.jpg",
    "ilse-orsel-v0oyDAu4Ypg-unsplash.jpg",
    "johannes-plenio-2TQwrtZnl08-unsplash.jpg"];
  let randNb = Math.floor(Math.random() * 7);
  document.getElementById("tuningbgImg")
    .style.setProperty("--gti-mode", "url(../medias/bg/" + randImg[randNb] + ")");
}, 3000);
