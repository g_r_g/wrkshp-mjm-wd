ZARA

ABOUT
Zara is a font designed during a Flixbus trip, from Besançon (FR) to Prague (CZ). The font is available in two styles: Regular and Oblique.
Designed in 2016 by the fonderie.download team, published in january 2020 by fonderie.download.

LICENSE
Zara is published under the WTFPL – Do What the Fuck You Want to Public License. For more informations, open the LICENSE.txt file or go to http://www.wtfpl.net/.

ABOUT FONDERIE.DOWNLOAD
We design and distribute typefaces. All our fonts are released under the WTFPL license.

CONTACT
http://www.fonderie.download/
contact@fonderie.download
