4 FROMAGES

ABOUT
4 Fromages is a display typeface which has been created during the first edition of Saturday Type Fever, a type-marathon hosted by NoFoundry back in 2018 in Karlsruhe. Designed with 8 hands by François Elain, lazy dog and fonderie.download. Published in june 2021 by fonderie.download.

LICENSE
4 fromages is published under the WTFPL – Do What the Fuck You Want to Public License. For more informations, open the LICENSE.txt file or go to http://www.wtfpl.net/.

ABOUT FONDERIE.DOWNLOAD
We design and distribute typefaces. All our fonts are released under the WTFPL license.

CONTACT
https://elainfrancois.fr/
https://www.julesdurand.xyz/
http://www.fonderie.download/
contact@fonderie.download
