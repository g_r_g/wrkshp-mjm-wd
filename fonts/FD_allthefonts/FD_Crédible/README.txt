CRÉDIBLE

ABOUT
For the streets, by the streets. Crédible is made of glyphs collected from diverse tags and graffiti found in the streets. The font is available in one style with three different stylistic sets and several weird ligatures.
Designed in 2019 by the fonderie.download team, published in january 2020 by fonderie.download.

LICENSE
Crédible is published under the WTFPL – Do What the Fuck You Want to Public License. For more informations, open the LICENSE.txt file or go to http://www.wtfpl.net/.

ABOUT FONDERIE.DOWNLOAD
We design and distribute typefaces. All our fonts are released under the WTFPL license.

CONTACT
http://www.fonderie.download/
contact@fonderie.download
