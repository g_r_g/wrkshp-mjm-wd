Tribalium Neue

ABOUT
Improved version of the Tribalium font, still designed in order to torture your eyes, but with lower cases too! The font is available in two styles: Regular and Poster. Both styles will be equally useful for your next tattoo idea!
Designed in 2020 by the fonderie.download team, published in july 2020 by fonderie.download.

LICENSE
Tribalium is published under the WTFPL – Do What the Fuck You Want to Public License. For more informations, open the LICENSE.txt file or go to http://www.wtfpl.net/.

ABOUT FONDERIE.DOWNLOAD
We design and distribute typefaces. All our fonts are released under the WTFPL license.

CONTACT
http://www.fonderie.download/
contact@fonderie.download
